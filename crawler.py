#scrypt DO NOT require python 3

from bs4 import BeautifulSoup
import requests
import requests.exceptions
from urlparse import urlparse
from collections import deque
import re
 
emailsFile = open('emailList.lst', 'w', 0)
urlsFile = open('urlList.lst', 'w', 0)

# a deque of urls, set initial url seed
urlsToProcess = deque(['https://moz.com/top500'])
 
# urls processed!
processed_urls = set()

emails = set()
 
# do until exist urls to process
while len(urlsToProcess):
 
    
    url = urlsToProcess.popleft()
    processed_urls.add(url)
     
    urlsFile.write(url + "\n")
    
    parts = urlparse(url)
    base_url = "{0.scheme}://{0.netloc}".format(parts)
    path = url[:url.rfind('/')+1] if '/' in parts.path else url
 
    print("Crawling site: %s" % url)
    try:
        response = requests.get(url)
    except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
       
        continue

    new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
    
    for email in new_emails:
        if email not in emails:
            emailsFile.write(email + "\n")
            emails.add(email)
 
    
    soup = BeautifulSoup(response.text)
 
    
    for anchor in soup.find_all("a"):
        # extract link
        link = anchor.attrs["href"] if "href" in anchor.attrs else ''
        if link.startswith('/'):
            link = base_url + link
        elif not link.startswith('http'):
            link = path + link
        # add url to the the list
        if not link in urlsToProcess and not link in processed_urls:
            urlsToProcess.append(link)
